-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Хост: std-mysql
-- Время создания: Июл 08 2022 г., 09:39
-- Версия сервера: 5.7.26-0ubuntu0.16.04.1
-- Версия PHP: 8.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `std_1945_quotes`
--

-- --------------------------------------------------------

--
-- Структура таблицы `quote`
--

CREATE TABLE `quote` (
  `id` int(11) NOT NULL,
  `teacher` varchar(255) NOT NULL,
  `subject` varchar(32) NOT NULL,
  `date` date NOT NULL,
  `user_id` int(11) NOT NULL,
  `quote` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `quote`
--

INSERT INTO `quote` (`id`, `teacher`, `subject`, `date`, `user_id`, `quote`) VALUES
(1, 'Test', 'Test', '2022-06-21', 2, 'Не переживайте, всё будет плохо'),
(2, 'Test', 'Test', '2022-06-21', 2, 'Teeeeeeeeeest'),
(3, 'Test', 'Test', '2022-06-21', 2, 'Test'),
(9, 'Добавление', 'Цитаты', '2022-07-07', 2, 'Тестируем'),
(11, 'Обычного', 'Пользователя', '2022-07-07', 19, 'Тестируем');

-- --------------------------------------------------------

--
-- Структура таблицы `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `role` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `roles`
--

INSERT INTO `roles` (`id`, `role`) VALUES
(2, 'common'),
(4, 'guest'),
(1, 'superuser'),
(3, 'verifier');

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `login` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` int(11) DEFAULT NULL,
  `group` varchar(16) NOT NULL DEFAULT '211-363'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `login`, `password`, `role`, `group`) VALUES
(2, 'Ender', '65564:87120847225caae063f6048977aed181:568988fb8d9bc6d97628132399770ce500972bbe4e3c12ad0549ba5210bc921cbd803cadf1eff66c5f9173c93dd00756532e92bd8cded25222a385621a6cd45f', 1, '211-363'),
(18, 'Verifier', '65564:d0686dba594d78e193cfaf9b2d17605c:d79cd8e21ca4974b093975aef23253d962e9b9cb421519990a0b0ba15aa5f60c8db269614fece2889b102fbab046da4850711a2cdefd3b0ed1eedb94a73e47b2', 3, '211-363'),
(19, 'Test', '65564:8de7c7e133e9e95697854f82de720f4a:f2c79682d5c4f739ce813feadd3e51abe9e94b662fa3e144f5725c7530dd69b77c0af1484171479483271391c622444789e5c72fd977c75b066ad4f0752802bf', 2, '211-363');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `quote`
--
ALTER TABLE `quote`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `role` (`role`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `login` (`login`),
  ADD KEY `role` (`role`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `quote`
--
ALTER TABLE `quote`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT для таблицы `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `quote`
--
ALTER TABLE `quote`
  ADD CONSTRAINT `quote_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`role`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
