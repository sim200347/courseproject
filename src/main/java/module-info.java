module com.ender.courseproject {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.web;

    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;
    requires validatorfx;
    requires org.kordamp.ikonli.javafx;
    requires org.kordamp.bootstrapfx.core;
    requires eu.hansolo.tilesfx;
    requires java.sql;
    requires mysql.connector.java;
    requires MaterialFX;

    opens com.ender.courseproject to javafx.fxml;
    exports com.ender.courseproject;
    exports com.ender.courseproject.controller;
    opens com.ender.courseproject.controller to javafx.fxml;
    exports com.ender.courseproject.model;
    opens com.ender.courseproject.model to javafx.fxml;
}