package com.ender.courseproject;

import com.ender.courseproject.model.CustomStatement;

import java.sql.ResultSet;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Main {
    public static void main(String[] args) {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            CustomStatement statement = new CustomStatement();
            String query = "roles";
            ResultSet result = statement.getTableFields(query);
            while (result.next()) {
                int id = result.getInt("id");
                String name = result.getString("role");
                System.out.print("id = " + id);
                System.out.println(", role = \"" + name + "\"");
            }
//            System.out.println(statement.addUser("Ender", "200347ATs", 1));
            System.out.println(statement.login("Ender", "200347ATs"));
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("uuuu-MM-dd");
            System.out.println(statement.addQuote("Test", "Test", "Test", dtf.format(LocalDate.now()), "Ender"));
            result = statement.getQuotesByUser("Ender");
            while (result.next()) {
                System.out.println(result.getString("quote"));
            }
            System.out.println(statement.countQuotesByUser("Ender"));
            System.out.println(statement.updateLogin("Ender"));
            System.out.println(statement.updatePassword("Ender", "200347AT"));
            System.out.println(statement.updateRole("Ender", "guest"));
            System.out.println(statement.login("Ender", "200347ATs"));
            System.out.println(statement.login("Ender", "200347AT"));
            System.out.println(statement.updateRole("Ender", "superuser"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
