package com.ender.courseproject.model;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.sql.*;

public class CustomStatement {
    Connection connection = DriverManager.getConnection(
            "jdbc:mysql://std-mysql.ist.mospolytech.ru:3306/std_1945_quotes",
            "std_1945_quotes", "200347ATs");

    public CustomStatement() throws SQLException {
    }

    private static String generateStrongPasswordHash(String password)
            throws NoSuchAlgorithmException, InvalidKeySpecException {
        int iterations = 65564;
        char[] chars = password.toCharArray();
        byte[] salt = getSalt();
        PBEKeySpec spec = new PBEKeySpec(chars, salt, iterations, 64 * 8);
        SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        byte[] hash = skf.generateSecret(spec).getEncoded();
        return iterations + ":" + toHex(salt) + ":" + toHex(hash);
    }

    private static byte[] getSalt() throws NoSuchAlgorithmException {
        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
        byte[] salt = new byte[16];
        sr.nextBytes(salt);
        return salt;
    }

    private static String toHex(byte[] array) {
        BigInteger bi = new BigInteger(1, array);
        String hex = bi.toString(16);
        int paddingLength = (array.length * 2) - hex.length();
        if (paddingLength > 0) {
            return String.format("%0" + paddingLength + "d", 0) + hex;
        } else {
            return hex;
        }
    }

    private static boolean validatePassword(String originalPassword, String storedPassword)
            throws NoSuchAlgorithmException, InvalidKeySpecException {
        String[] parts = storedPassword.split(":");
        int iterations = Integer.parseInt(parts[0]);
        byte[] salt = fromHex(parts[1]);
        byte[] hash = fromHex(parts[2]);
        PBEKeySpec spec = new PBEKeySpec(originalPassword.toCharArray(),
                salt, iterations, hash.length * 8);
        SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        byte[] testHash = skf.generateSecret(spec).getEncoded();
        int diff = hash.length ^ testHash.length;
        for (int i = 0; i < hash.length && i < testHash.length; i++) {
            diff |= hash[i] ^ testHash[i];
        }
        return diff == 0;
    }

    private static byte[] fromHex(String hex) {
        byte[] bytes = new byte[hex.length() / 2];
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = (byte) Integer.parseInt(hex.substring(2 * i, 2 * i + 2), 16);
        }
        return bytes;
    }

    public ResultSet getTableFields(String table) throws SQLException {
        Statement statement = connection.createStatement();
        return statement.executeQuery("SELECT * From %s Order By id".formatted(table));
    }

    public ResultSet getAllRoles() throws SQLException {
        Statement statement = connection.createStatement();
        return statement.executeQuery("SELECT role FROM roles where id != 1 && id != 4");
    }

    public ResultSet getQuotesByUser(String user) throws SQLException {
        String query = "SELECT quote FROM quote WHERE id = ?";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, getUserId(user));
        return statement.executeQuery();
    }

    public ResultSet getAllQuotes() throws SQLException {
        String query = "SELECT quote, teacher FROM quote";
        PreparedStatement statement = connection.prepareStatement(query);
        return statement.executeQuery();
    }

    public int countQuotesByUser(String user) throws SQLException {
        String query = "SELECT count(quote) FROM quote WHERE user_id = ? GROUP BY id";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, getUserId(user));
        ResultSet result = statement.executeQuery();
        result.next();
        return result.getInt(1);
    }

    public int countAllQuotes() throws SQLException {
        String query = "SELECT count(quote) FROM quote";
        PreparedStatement statement = connection.prepareStatement(query);
        ResultSet result = statement.executeQuery();
        result.next();
        return result.getInt(1);
    }

    public int addQuote(String quote, String teacher, String subject, String date, String user) throws SQLException {
        String query = "INSERT INTO quote (id, teacher, subject, date, user_id, quote) VALUES (?, ?, ?, ?, ?, ?)";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setNull(1, 1);
        statement.setString(2, teacher);
        statement.setString(3, subject);
        statement.setDate(4, Date.valueOf(date));
        statement.setInt(5, getUserId(user));
        statement.setString(6, quote);
        return statement.executeUpdate();
    }

    public int getUserId(String login) throws SQLException {
        String query = "SELECT id FROM user WHERE login = ?";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, login);
        ResultSet result = statement.executeQuery();
        result.next();
        return result.getInt("id");
    }

    public int getRoleId(String role) throws SQLException {
        String query = "SELECT id FROM roles WHERE role = ?";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, role);
        ResultSet result = statement.executeQuery();
        result.next();
        return result.getInt("id");
    }

    public int register(String login, String password, String role) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {
        String query = "INSERT INTO user  (id, login, password, role) VALUES (?, ?, ?, ?)";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setNull(1, 1);
        statement.setString(2, login);
        statement.setString(3, generateStrongPasswordHash(password));
        statement.setInt(4, getRoleId(role));
        return statement.executeUpdate();
    }


    public int updateLogin(String login) throws SQLException {
        String query = "UPDATE user SET user.login = ? WHERE id = ?";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, login);
        statement.setInt(2, getUserId(login));
        return statement.executeUpdate();
    }

    public int updatePassword(String login, String password) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {
        String query = "UPDATE user SET user.password = ? WHERE id = ?";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, generateStrongPasswordHash(password));
        statement.setInt(2, getUserId(login));
        return statement.executeUpdate();
    }

    public int updateRole(String login, String role) throws SQLException {
        String query = "UPDATE user SET user.role = ? WHERE id = ?";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, getRoleId(role));
        statement.setInt(2, getUserId(login));
        return statement.executeUpdate();
    }

    public boolean login(String login, String password) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {
        String query = "SELECT password FROM user WHERE login = ?";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, login);
        ResultSet result = statement.executeQuery();
        result.next();
        return validatePassword(password, result.getString("password"));
    }

    public String getUserRole(String login) throws SQLException {
        String query = "SELECT role FROM roles WHERE id IN (SELECT role FROM user WHERE login = ?)";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, login);
        ResultSet result = statement.executeQuery();
        result.next();
        return result.getString("role");
    }

    public ResultSet getAllUsers() throws SQLException {
        String query = "SELECT login, role FROM user";
        PreparedStatement statement = connection.prepareStatement(query);
        return statement.executeQuery();
    }

    public int countAllUsers() throws SQLException {
        String query = "SELECT count(login) FROM user";
        PreparedStatement statement = connection.prepareStatement(query);
        ResultSet result = statement.executeQuery();
        result.next();
        return result.getInt(1);
    }

    public void deleteUser(String element1) throws SQLException {
        String query = "DELETE FROM user WHERE login = ?";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, element1);
        statement.executeUpdate();
    }

    public int countUserQuotes(String login) throws SQLException {
        String query = "SELECT count(quote) FROM quote WHERE user_id IN (SELECT id FROM user WHERE login = ?)";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, login);
        ResultSet result = statement.executeQuery();
        result.next();
        return result.getInt(1);
    }

    public ResultSet getUserQuotes(String login) throws SQLException {
        String query = "SELECT id, quote FROM quote WHERE user_id IN (SELECT id FROM user WHERE login = ?)";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, login);
        return statement.executeQuery();
    }

    public void deleteQuote(int id) throws SQLException {
        String query = "DELETE FROM quote WHERE id = ?";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, id);
        statement.executeUpdate();
    }

    public void updateQuote(String quote, int id) throws SQLException {
        String query = "UPDATE quote SET quote = ? WHERE id = ?";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, quote);
        statement.setInt(2, id);
        statement.executeUpdate();
    }
}
