package com.ender.courseproject;

import com.ender.courseproject.controller.DefaultInitialization;
import javafx.application.Application;
import javafx.stage.Stage;

import java.io.IOException;

public class CourseProjectApplication extends Application {
    public static void main(String[] args) {
        launch();
    }

    @Override
    public void start(Stage stage) throws IOException {
        DefaultInitialization.init(stage, "fxml/login-view.fxml", this.getClass());
    }
}