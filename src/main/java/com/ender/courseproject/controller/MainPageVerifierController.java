package com.ender.courseproject.controller;

import com.ender.courseproject.model.CustomStatement;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.SQLException;

public class MainPageVerifierController extends DefaultInitialization {

    @FXML
    public StackPane quotesFeed;

    public void start(Stage stage) throws IOException {
        init(stage, "fxml/main-page-verifier.fxml", this.getClass());
    }

    @FXML
    private void initialize() {
        try {
            CustomStatement customStatement = new CustomStatement();
            this.quoteFeed(customStatement, quotesFeed);
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    public void quoteOnButtonClick(ActionEvent actionEvent) {
    }

    public void settingsOnButtonClick(ActionEvent actionEvent) {
    }
}
