package com.ender.courseproject.controller;

import com.ender.courseproject.model.CustomStatement;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;

public class LoginController {
    @FXML
    private TextField authLogin;
    @FXML
    private TextField authPassword;
    @FXML
    private Button authButton;
    @FXML
    private TextField regLogin;
    @FXML
    private TextField regPassword;
    @FXML
    private ComboBox<String> regChoice;
    @FXML
    private Button regButton;
    @FXML
    private Button guestButton;
    @FXML
    private Label authErrorLabel;
    @FXML
    private Label regErrorLabel;

    private CustomStatement customStatement;

    @FXML
    private void initialize() throws SQLException {
        customStatement = new CustomStatement();
        ResultSet resultSet = customStatement.getAllRoles();
        System.out.println(resultSet);
        while (resultSet.next()) {
            regChoice.getItems().addAll(resultSet.getString("role"));
        }
        regChoice.getSelectionModel().selectFirst();

        authButton.setOnAction(actionEvent -> {
            boolean isLogin;
            try {
                isLogin = customStatement.login(authLogin.getText(), authPassword.getText());
            } catch (SQLException | NoSuchAlgorithmException | InvalidKeySpecException e) {
                authErrorLabel.setOpacity(1);
                throw new RuntimeException(e);
            }
            if (isLogin) {
                try {
                    changeScene(customStatement.getUserRole(authLogin.getText()));
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            } else {
                authErrorLabel.setOpacity(1);
            }
        });

        guestButton.setOnAction(actionEvent -> {
            MainPageGuestController mainPageController = new MainPageGuestController();
            Stage stage = (Stage) authButton.getScene().getWindow();
            try {
                mainPageController.start(stage);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });

        regButton.setOnAction(actionEvent -> {
            int reg;
            try {
                reg = customStatement.register(regLogin.getText(), regPassword.getText(), regChoice.getValue());
            } catch (SQLException | NoSuchAlgorithmException | InvalidKeySpecException e) {
                regErrorLabel.setOpacity(1);
                throw new RuntimeException(e);
            }
            if (reg > 0)
                changeScene(regChoice.getValue());
        });
    }

    private void changeScene(String role) {
        DefaultInitialization controller = new DefaultInitialization();
        switch (role) {
            case "common" -> controller = new MainPageCommonController();
            case "superuser" -> controller = new MainPageSuperuserController();
            case "guest" -> controller = new MainPageGuestController();
            case "verifier" -> controller = new MainPageVerifierController();
        }
        Stage stage = (Stage) authButton.getScene().getWindow();
        try {
            controller.setLogin(!Objects.equals(authLogin.getText(), "") ? authLogin.getText() : regLogin.getText());
            controller.setPassword(!Objects.equals(authPassword.getText(), "") ? authPassword.getText() : regPassword.getText());
            controller.start(stage);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
