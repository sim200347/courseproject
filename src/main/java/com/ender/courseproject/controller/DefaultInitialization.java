package com.ender.courseproject.controller;

import com.ender.courseproject.CourseProjectApplication;
import com.ender.courseproject.model.CustomStatement;
import io.github.palexdev.materialfx.controls.MFXButton;
import io.github.palexdev.materialfx.controls.MFXDatePicker;
import io.github.palexdev.materialfx.controls.MFXScrollPane;
import io.github.palexdev.materialfx.controls.MFXTextField;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DefaultInitialization {
    private static String login;
    private static String password;

    public static void init(Stage stage, String fileName, Class<?> className) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(CourseProjectApplication.class.getResource(fileName));
        Parent root = fxmlLoader.load();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        InputStream iconStream = className.getResourceAsStream("/images/quote-svgrepo-com.png");
        assert iconStream != null;
        Image image = new Image(iconStream);
        stage.getIcons().add(image);
        stage.setTitle("Quote");
        stage.setScene(scene);
        stage.show();
    }

    public void start(Stage stage) throws IOException {

    }

    public void setLogin(String login) {
        DefaultInitialization.login = login;
    }

    public void setPassword(String password) {
        DefaultInitialization.password = password;
    }

    protected void quoteFeed(CustomStatement customStatement, StackPane contentPane) throws SQLException {
        ResultSet resultSet = customStatement.getAllQuotes();
        MFXScrollPane mfxScrollPane = new MFXScrollPane();
        contentPane.getChildren().add(mfxScrollPane);
        GridPane feedGrid = new GridPane();
        VBox feed = new VBox();
        if (login != null) {
            Label statistics = new Label(("Количество цитат пользователя %s: %d".formatted(login, customStatement.countUserQuotes(login))));
            statistics.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));
            feed.getChildren().add(statistics);
        }
        mfxScrollPane.setContent(feedGrid);
        for (int i = 0; i < customStatement.countAllQuotes(); i++) {
            resultSet.next();
            Label quote = new Label();
            Label teacher = new Label();
            quote.setText(resultSet.getString(1));
            teacher.setText("Автор цитаты: " + resultSet.getString(2));
            generateGridPaneQuote(feed, quote, teacher, i);
        }
        feedGrid.add(feed, 0, 0);
        if (login != null) {
            GridPane addQuote = new GridPane();
            addQuote.setPadding(new Insets(20, 20, 20, 20));
            Label quote = new Label("Цитата");
            MFXTextField quoteText = new MFXTextField();
            quoteText.setPrefWidth(259);
            addQuote.add(quote, 0, 0);
            addQuote.add(quoteText, 1, 0);
            Label teacher = new Label("Преподаватель");
            MFXTextField teacherText = new MFXTextField();
            teacherText.setPrefWidth(259);
            addQuote.add(teacher, 0, 1);
            addQuote.add(teacherText, 1, 1);
            Label subject = new Label("Предмет");
            MFXTextField subjectText = new MFXTextField();
            subjectText.setPrefWidth(259);
            addQuote.add(subject, 0, 2);
            addQuote.add(subjectText, 1, 2);
            Label date = new Label("Дата");
            MFXDatePicker datePicker = new MFXDatePicker();
            addQuote.add(date, 0, 3);
            addQuote.add(datePicker, 1, 3);
            MFXButton add = new MFXButton("Сохранить");
            add.setOnAction(actionEvent -> {
                try {
                    customStatement.addQuote(quoteText.getText(), teacherText.getText(), subjectText.getText(), String.valueOf(datePicker.getValue()), login);
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            });
            addQuote.add(add, 0, 4);
            feedGrid.add(addQuote, 1, 0);
        }
    }

    protected void usersFeed(CustomStatement customStatement, StackPane contentPane) throws SQLException {
        ResultSet resultSet = customStatement.getAllUsers();
        MFXScrollPane mfxScrollPane = new MFXScrollPane();
        contentPane.getChildren().add(mfxScrollPane);
        VBox feed = new VBox();
        mfxScrollPane.setContent(feed);
        for (int i = 0; i < customStatement.countAllUsers(); i++) {
            resultSet.next();
            Label login = new Label();
            Label role = new Label();
            login.setText(resultSet.getString(1));
            role.setText("Роль: " + switch (resultSet.getInt(2)) {
                case 1 -> "superuser";
                case 2 -> "common user";
                case 3 -> "verifier";
                default -> throw new IllegalStateException("Unexpected value: " + resultSet.getInt("role"));
            });
            generateGridPaneUser(customStatement, feed, login, role, i);
        }
    }

    protected void settings(CustomStatement customStatement, StackPane contentPane) throws SQLException {
        GridPane settingsGrid = new GridPane();
        GridPane gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 10, 10, 10));
        MFXTextField loginLabel = new MFXTextField(login);
        MFXTextField passwordLabel = new MFXTextField(password);
        gridPane.add(new Label("Логин"), 0, 0);
        gridPane.add(loginLabel, 0, 1);
        gridPane.add(new Label("Пароль"), 1, 0);
        gridPane.add(passwordLabel, 1, 1);
        MFXButton confirm = new MFXButton("Сохранить");
        gridPane.add(confirm, 0, 2);
        confirm.setOnAction(actionEvent -> {
            try {
                customStatement.updateLogin(loginLabel.getText());
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
            try {
                customStatement.updatePassword(loginLabel.getText(), passwordLabel.getText());
            } catch (SQLException | NoSuchAlgorithmException | InvalidKeySpecException e) {
                throw new RuntimeException(e);
            }
        });
        settingsGrid.add(gridPane, 0, 0);
        GridPane quoteList = new GridPane();
        ResultSet resultSet = customStatement.getUserQuotes(login);
        for (int i = 0; i < customStatement.countUserQuotes(login); i++) {
            GridPane listElement = new GridPane();
            listElement.setPadding(new Insets(10, 10, 10, 10));
            resultSet.next();
            MFXTextField quoteText = new MFXTextField(resultSet.getString(2));
            quoteText.setPrefWidth(259);
            listElement.add(quoteText, 0, 0);
            MFXButton delete = new MFXButton("Удалить");
            delete.setId("deleteQuote_" + resultSet.getInt(1));
            MFXButton update = new MFXButton("Обновить");
            update.setId("updateQuote_" + resultSet.getInt(1));
            delete.setOnAction(actionEvent -> {
                try {
                    customStatement.deleteQuote(Integer.parseInt(update.getId().split("_")[1]));
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            });
            update.setOnAction(actionEvent -> {
                try {
                    customStatement.updateQuote(quoteText.getText(), Integer.parseInt(update.getId().split("_")[1]));
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            });
            listElement.add(delete, 0, 1);
            listElement.add(update, 1, 0);
            quoteList.add(listElement, 0, i);
        }
        settingsGrid.add(quoteList, 1, 0);
        MFXScrollPane mfxScrollPane = new MFXScrollPane();
        contentPane.getChildren().add(mfxScrollPane);
        mfxScrollPane.setContent(settingsGrid);
    }

    private void generateGridPaneUser(CustomStatement customStatement, VBox feed, Label element1, Label element2, int index) {
        GridPane quotePane = new GridPane();
        GridPane quoteElement = new GridPane();
        MFXButton mfxButton = new MFXButton("Удалить");
        mfxButton.setId("deleteUser_" + index);
        mfxButton.setOnAction(actionEvent -> {
            try {
                customStatement.deleteUser(element1.getText());
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        });
        quoteElement.add(element1, 0, 0);
        quoteElement.add(element2, 0, 1);
        quoteElement.add(mfxButton, 0, 2);
        generateGrid(quoteElement, quotePane, index, feed);
    }

    private void generateGridPaneQuote(VBox feed, Label element1, Label element2, int index) {
        GridPane quotePane = new GridPane();
        GridPane quoteElement = new GridPane();
        quoteElement.add(element1, 0, 0);
        quoteElement.add(element2, 0, 1);
        generateGrid(quoteElement, quotePane, index, feed);
    }

    private void generateGrid(GridPane quoteElement, GridPane quotePane, int index, VBox feed) {
        quoteElement.setPadding(new Insets(10, 10, 10, 10));
        quotePane.add(quoteElement, 0, index + 1);
        GridPane.setMargin(quotePane, new Insets(50, 50, 50, 50));
        feed.getChildren().add(quotePane);
        feed.setBackground(new Background(new BackgroundFill(Color.GRAY, CornerRadii.EMPTY, Insets.EMPTY)));
        quotePane.setBorder(new Border(new BorderStroke(Color.BLACK,
                BorderStrokeStyle.SOLID,
                CornerRadii.EMPTY,
                BorderWidths.DEFAULT)));
        quotePane.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));
    }
}
