package com.ender.courseproject.controller;

import com.ender.courseproject.model.CustomStatement;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.SQLException;

public class MainPageSuperuserController extends DefaultInitialization {
    @FXML
    public VBox quotesFeed;
    @FXML
    public StackPane contentPane;

    CustomStatement customStatement;

    public void start(Stage stage) throws IOException {
        init(stage, "fxml/main-page-superuser.fxml", this.getClass());
    }

    @FXML
    private void initialize() {
        try {
            customStatement = new CustomStatement();
            this.quoteFeed(customStatement, contentPane);
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    public void quoteOnButtonClick(ActionEvent actionEvent) {
        contentPane.getChildren().removeAll();
        try {
            this.quoteFeed(customStatement, contentPane);
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    public void userOnButtonClick(ActionEvent actionEvent) {
        contentPane.getChildren().removeAll();
        try {
            this.usersFeed(customStatement, contentPane);
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    public void settingsOnButtonClick(ActionEvent actionEvent) {
        contentPane.getChildren().removeAll();
        try {
            this.settings(customStatement, contentPane);
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }
}
